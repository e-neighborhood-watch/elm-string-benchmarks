module StringJoin exposing
  ( main
  )


import Benchmark exposing
  ( Benchmark
  )
import Benchmark.Runner as Runner
import Util.List as List


betterJoinHelper : String -> List String -> String -> String
betterJoinHelper sep strs acc =
  case
    strs
  of
    [] ->
      acc


    str :: rest ->
      betterJoinHelper
        sep
        rest
        ( String.append
          ( String.append acc sep
          )
          str
        )


betterJoin : String -> List String -> String
betterJoin sep strs =
  case
    strs
  of
    [] ->
      ""


    str :: rest ->
      betterJoinHelper sep rest str


type alias Lengths =
  { separator : Int
  , string : Int
  , list : Int
  }


lengthsToString : Lengths -> String
lengthsToString { separator, string, list } =
  "("
    ++ String.fromInt separator
    ++ ","
    ++ String.fromInt string
    ++ ","
    ++ String.fromInt list
    ++ ")"


type alias TestData =
  { lengths : Lengths
  , separator : String
  , strings : List String
  }


testData : Int -> Int -> Int -> TestData
testData separatorLength stringLength listLength =
  { lengths =
    { separator =
      separatorLength
    , string =
      stringLength
    , list =
      listLength
    }
  , separator =
    String.repeat separatorLength ","
  , strings =
    String.repeat stringLength "a"
      |> List.repeat listLength
  }


tests : List TestData
tests =
  List.singleton testData
    |> List.andMap [ 0, 1, 10, 100, 1000 ]
    |> List.andMap [ 1, 10, 100, 1000, 10000 ]
    |> List.andMap [ 10, 100, 500, 1000, 5000 ]


compareStringJoin : TestData -> Benchmark
compareStringJoin { lengths, separator, strings } =
  Benchmark.compare
    ( lengthsToString lengths
    )
    "String.join"
    ( \ _ -> String.join separator strings
    )
    "betterJoin"
    ( \ _ -> betterJoin separator strings
    )


main : Runner.BenchmarkProgram
main =
  tests
    |> List.map compareStringJoin
    |> Benchmark.describe "String Join. Lengths: (separator, string, list)"
    |> Runner.program
