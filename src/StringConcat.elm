module StringConcat exposing
  ( main
  )


import Benchmark exposing
  ( Benchmark
  )
import Benchmark.Runner as Runner
import Util.List as List


betterConcatHelper : List String -> String -> String
betterConcatHelper strs acc =
  case
    strs
  of
    [] ->
      acc

    str :: rest ->
      betterConcatHelper
        rest
        ( String.append
          acc
          str
        )


betterConcat : List String -> String
betterConcat strs =
  betterConcatHelper strs ""


type alias Lengths =
  { string : Int
  , list : Int
  }


lengthsToString : Lengths -> String
lengthsToString { string, list } =
  "("
    ++ String.fromInt string
    ++ ","
    ++ String.fromInt list
    ++ ")"


type alias TestData =
  { lengths : Lengths
  , strings : List String
  }


testData : Int -> Int -> TestData
testData stringLength listLength =
  { lengths =
    { string =
      stringLength
    , list =
      listLength
    }
  , strings =
    String.repeat stringLength "b"
      |> List.repeat listLength
  }


tests : List TestData
tests =
  List.singleton testData
    |> List.andMap [ 1, 10, 100, 500, 1000, 5000, 10000 ]
    |> List.andMap [ 10, 50, 100, 500, 1000, 5000, 10000 ]


compareStringConcat : TestData -> Benchmark
compareStringConcat { lengths, strings } =
  Benchmark.compare
    ( lengthsToString lengths
    )
    "String.concat"
    ( \ _ -> String.concat strings
    )
    "betterConcat"
    ( \ _ -> betterConcat strings
    )


main : Runner.BenchmarkProgram
main =
  tests
    |> List.map compareStringConcat
    |> Benchmark.describe "String Concat. Lengths: (string, list)"
    |> Runner.program
