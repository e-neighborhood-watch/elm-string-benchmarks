module Util.List exposing
  ( andMap
  )


andMap : List a -> List (a -> b) -> List b
andMap list =
  List.concatMap
    ( \ function ->
      List.map function list
    )
