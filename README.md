# Elm String Benchmarks

Some benchmarks to test alternate implementations for some functions in Elm's [`String`](https://package.elm-lang.org/packages/elm/core/latest/String) module.

Be warned that some will take a *long* time to run.

## [`String.join`][String.join]

The [library version](https://github.com/elm/core/blob/8e99db3fcbb101838c36d534a7be7add6b6d739e/src/String.elm#L200)
of this function is implemented by a call to the Javascript function `join`,
but it seems like a pure Elm version is actually much faster across the board.

#### Alternate Join

```elm
joinHelper : String -> List String -> String -> String
joinHelper sep strs acc =
  case
    strs
  of
    [] ->
      acc


    str :: rest ->
      joinHelper
        sep
        rest
        ( String.append
          ( String.append acc sep
          )
          str
        )


join : String -> List String -> String
join sep strs =
  case
    strs
  of
    [] ->
      ""


    str :: rest ->
      joinHelper sep rest str

```

#### Join Results

[Try it yourself!](https://e-neighborhood-watch.gitlab.io/elm-string-benchmarks/join.html)
(`elm make` flags: `--optimize`)

Expected benchmark run time: ~7 hours

| Platform                                                                                                                          | Alternate Faster?  |
| --------                                                                                                                          | -----------------  |
| [Chromium Version 87.0.4280.141 (Developer Build) built on Debian 10.7, running on Debian 10.7 (64-bit) \[elm reactor\]][join1]\* | :heavy_check_mark: |

\* This result was from an earlier version of the benchmark code before it was cleaned up.
The benchmark tests and alternate function were the same in the previous version, only aesthetic changes were made.

[join1]: https://e-neighborhood-watch.gitlab.io/elm-string-benchmarks/results/join/1.html

## [`String.concat`](https://package.elm-lang.org/packages/elm/core/latest/String#concat)

The [library version](https://github.com/elm/core/blob/84f38891468e8e153fc85a9b63bdafd81b24664e/src/String.elm#L178)
of this function just makes a call to [`String.join`][String.join] with the separator being the empty string,
and so it is not unreasonable to expect that an alternate implementation could be faster.

#### Alternate Concat

```elm
concatHelper : List String -> String -> String
concatHelper strs acc =
  case
    strs
  of
    [] ->
      acc

    str :: rest ->
      concatHelper
        rest
        ( String.append
          acc
          str
        )


concat : List String -> String
concat strs =
  concatHelper strs ""
```

#### Concat Results

[Try it yourself!](https://e-neighborhood-watch.gitlab.io/elm-string-benchmarks/concat.html)
(`elm make` flags: `--optimize`)

| Platform                                                                                                        | Alternate Faster?  |
| --------                                                                                                        | -----------------  |
| [Chromium Version 87.0.4280.141 (Developer Build) built on Debian 10.7, running on Debian 10.7 (64-bit)][join1] | :heavy_check_mark: |

[concat1]: https://e-neighborhood-watch.gitlab.io/elm-string-benchmarks/results/concat/1.html

[String.join]: https://package.elm-lang.org/packages/elm/core/latest/String#join
